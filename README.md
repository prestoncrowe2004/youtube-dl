# youtube-d(own)l(oader) - Just remove the text in parentheses to get the original!

I know it's a pain for all of the commands, but bear with me, I'm avoiding a YMCA ;).

I don't want to risk a YMCA at all, so I am only linking to the old readme.
- Wayback Machine version of OG GH: https://web.archive.org/web/20201015142501/https://github.com/ytdl-org/youtube-dl
- Pastebin with raw text (disregard content warning): https://pastebin.com/XSSGuuhS
- You can also check the original repo this is forked from.